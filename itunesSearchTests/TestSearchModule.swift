//
//  TestSearchModule.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Foundation
import Mockit

class TestSearchModule: XCTestCase {
    var coreNetwork: MockCoreNetwork<SearchResult>!
    var presenter: MockSearchPresenter!
    var interator: SearchInteractorImpl!
    var view: MockSearchView!
    var searchingEntities: [EntityType] = EntityType.alltypes
    var expextedSections: [Section] = []
    var expextedItems: [Item] = []
    var expextedCount: Int = 0
    let requiredInputErrorMessage = "Please provide a valid value for search term " +
        "and select one ore more category"
    
    func updateExpectedResults() {
        var sections: [Section] = []
        for entity in searchingEntities {
            let items = (1...Int.random(in: (1...10)))
                .map({ Item(entityType: entity.title, wrapperType: "",
                            artistName: "", previewUrl: "", artworkUrl100: "", trackName: "item\($0)")})
            expextedCount += items.count
            self.expextedItems.append(contentsOf: items)
            let section = Section(title: entity.title, items: items)
            sections.append(section)
        }
        self.expextedSections = sections
    }
    
    override func setUpWithError() throws {
        expextedCount = 0
        updateExpectedResults()
        view = MockSearchView.init(testCase: self)
        presenter = MockSearchPresenter(view: view, testCase: self)
        coreNetwork = MockCoreNetwork<SearchResult>()
        interator = SearchInteractorImpl(presenter: presenter,
                                         service: SearchService(coreNetwork: coreNetwork))
    }
    
    func testGroupingLogic() {
        let r = SearchResult(resultCount: expextedItems.count, results: expextedItems)
        assert(r.sections.allSatisfy({ resultSection in
            expextedSections.contains(where: { expected in
                resultSection.items.count == expected.items.count &&
                    resultSection.items.allSatisfy({ item in
                        expected.items.contains(item)
                    })
            })
        }))
    }
    
    func testdidLoadSearchResults() throws {
        let result = SearchResult.init(resultCount: expextedCount, results: expextedItems)
        coreNetwork?.result = result
        coreNetwork?.error = nil
        interator?.search(searchTerm: "valid", categories: searchingEntities)
        wait(for: 3)
        presenter?.verify(verificationMode: AtLeastOnce()).interactor(didLoadSearchResults: result)
        view?.verify(verificationMode: AtLeastOnce()).hideLoadingView()
        view?.verify(verificationMode: AtLeastOnce()).showSearchResultsScreen(with: result)
    }
    
    func testConfigurator() throws {
        guard let viewController = SearchViewController.loadFromNib() else { assert(false) }
        SearchConfigurator().configure(viewController: viewController)
        assert(viewController.interactor != nil)
    }
    
    func testEmptyCategory() throws {
        interator?.search(searchTerm: "valid", categories: [])
        view?.verify(verificationMode: AtLeastOnce())
            .hideLoadingView()
        view?.verify(verificationMode: AtLeastOnce())
            .showErrorMessage(requiredInputErrorMessage)
    }
    
    func testEmptyTerm() throws {
        interator?.search(searchTerm: "", categories: searchingEntities)
        view?.verify(verificationMode: Once()).hideLoadingView()
        view?.verify(verificationMode: AtLeastOnce())
            .showErrorMessage(requiredInputErrorMessage)
    }
    
    func testFailedToLoadSearchResults() throws {
        coreNetwork?.result = nil
        coreNetwork?.error = NSError(domain: "", code: 400,
                                     userInfo: [ NSLocalizedDescriptionKey: "Failed To Load Data"])
        interator?.search(searchTerm: "valid", categories: searchingEntities)
        wait(for: 3)
        view?.verify(verificationMode: Once()).hideLoadingView()
        view?.verify(verificationMode: Once()).showErrorMessage("Failed To Load Data")
        presenter?.verify(verificationMode: Once()).interactor(didFailToLoadToSearchResults: "Failed To Load Data")
    }
    
    func testViewController() throws {
        guard let viewController = SearchViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let parent = UINavigationController.init(rootViewController: viewController)
        viewController.showLoadingView()
        assert(!viewController.loadingView.isHidden)
        viewController.hideLoadingView()
        assert(viewController.loadingView.isHidden)
        viewController.didSelectCategories(EntityType.alltypes)
        assert(viewController.collectionView(viewController.categoriesCollection,
                                             numberOfItemsInSection: 0) == EntityType.alltypes.count)
        viewController.viewWillAppear(false)
        assert(parent.isNavigationBarHidden)
        viewController.viewWillDisappear(false)
        assert(!parent.isNavigationBarHidden)
        viewController.searchTermField.text = ""
        viewController.interactor = interator
        viewController.didClickOnSearch(self)
        wait(for: 3)
        presenter.verify(verificationMode: Once())
            .interactor(didFailToLoadToSearchResults: requiredInputErrorMessage)
    }
}

class MockSearchView: SearchView, Mock {
    func showErrorMessage(_ error: String) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: error)
    }
    
    func showSearchResultsScreen(with results: SearchResult) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: results)
    }
    
    func hideLoadingView() {
        callHandler.accept(nil, ofFunction: #function, atFile: #file, inLine: #line, withArgs: nil)
    }
    
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockSearchView {
        return self
    }
}

class MockCoreNetwork<Result: Codable>: CoreNetworkProtocol {
    var result: Result?
    var error: Error?
    
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        if let result = self.result as? T {
            completion(result, error)
        } else {
            completion(nil, self.error)
            
        }
    }
}

extension XCTestCase {
    func wait(for seconds: Double) {
        let expectation = XCTestExpectation.init()
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
            expectation.fulfill()
        })
        wait(for: [expectation], timeout: (seconds + 2))
    }
}

class MockSearchPresenter: SearchPresenterImpl, Mock {
    let callHandler: CallHandler
    
    init(view: SearchView, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view)
    }
    
    func instanceType() -> MockSearchPresenter {
        return self
    }
    
    override func interactor(didFailToLoadToSearchResults error: String) {
        super.interactor(didFailToLoadToSearchResults: error)
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: error)
    }
    
    override func interactor(didLoadSearchResults results: SearchResult) {
        super.interactor(didLoadSearchResults: results)
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: results)
    }
    
}
