//
//  TestAlamofireAdaptor.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Alamofire

class TestAlamofireAdaptor: XCTestCase {
    
    func testAdapter() {
        let adapter = AlamofireAdaptor(baseURL: "test")
        assert(adapter.convertRequestEnconding(.json) is JSONEncoding)
        assert(adapter.convertRequestEnconding(.urlEncodedInURL) is URLEncoding)
        assert(adapter.convertRequestMethodToAlamofireMethod(.GET) == HTTPMethod.get)
        assert(adapter.convertRequestMethodToAlamofireMethod(.DELETE) == HTTPMethod.delete)
        assert(adapter.convertRequestMethodToAlamofireMethod(.PUT) == HTTPMethod.put)
        assert(adapter.convertRequestMethodToAlamofireMethod(.POST) == HTTPMethod.post)
    }
}
