//
//  TestCells.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest

class TestCells: XCTestCase {
    
    func testResultItemListCell() {
        let cell: ResultItemListCell? = self.createView(nib: ResultItemListCell.nibName)
        guard let cellValue = cell else { return XCTFail("failed") }
        cellValue.configure(item: Item(entityType: "", wrapperType: "",
                                       artistName: "Artist", previewUrl: "",
                                       artworkUrl100: "www.google.com", trackName: "Title"))
        assert(cellValue.titleLabel.text == "Title")
        assert(cellValue.artistLabel.text == "Artist")
        cellValue.configure(item: Item(entityType: "",
                                       wrapperType: "", artistName: "Artist",
                                       previewUrl: "", artworkUrl100: "", trackName: nil))
        assert(cellValue.titleLabel.text == "Artist")
        assert(cellValue.artistLabel.text == nil)
        cell?.prepareForReuse()
        assert(cell?.image.image == nil)
    }
    
    func testResultGridCell() {
        let cell: ResultItemGridCell? = self.createView(nib: ResultItemGridCell.nibName)
        guard let cellValue = cell else { return XCTFail("failed") }
        cellValue.configure(item: Item(entityType: "", wrapperType: "",
                                            artistName: "", previewUrl: "",
                                            artworkUrl100: "www.google.com", trackName: "Title"))
        assert(cellValue.titlelabel.text == "Title")
        cellValue.configure(item: Item(entityType: "",
                                       wrapperType: "", artistName: "Artist",
                                       previewUrl: "", artworkUrl100: "",
                                       trackName: nil))
        assert(cellValue.titlelabel.text == "Artist")
        cell?.prepareForReuse()
        assert(cell?.image.image == nil)
    }
    
    func testCategoryTableViewCell() {
        let cell: CategoryTableViewCell? = self.createView(nib: CategoryTableViewCell.nibName)
        guard let cellValue = cell else { return XCTFail("failed") }
        var category = EntityType(selected: false, title: "TestTitle")
        cellValue.configure(category: category)
        assert(cellValue.titleLabel.text == category.title)
        assert(cellValue.checkedImage.isHidden)
        category.selected = true
        cellValue.configure(category: category)
        assert(!cellValue.checkedImage.isHidden)
    }
    
    func testCategoryCell() {
        let cell: CategoryCell? = self.createView(nib: CategoryCell.nibName)
        guard let cellValue = cell else { return XCTFail("failed") }
        cellValue.configure(title: "Title1")
        assert(cellValue.titleLabel.text == "Title1")
    }
    
    func testSectionHeader() {
        let cell: ResultsSectionHeader? = self.createView(nib: ResultsSectionHeader.nibName)
        guard let cellValue = cell else { return XCTFail("failed") }
        cellValue.configure(title: "Title1")
        assert(cellValue.titleLabel.text == "Title1")
    }
}

extension  XCTestCase {
    func createView<T: UIView>(nib: String) -> T {
        let bundle = Bundle(for: T.self)
        guard let view = bundle.loadNibNamed(nib, owner: nil)?.first as? T else {
            assert(false)
        }
        return view
    }
}
