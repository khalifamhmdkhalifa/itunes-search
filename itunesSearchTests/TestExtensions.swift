//
//  TestExtensions.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Mockit

class TestExtensions: XCTestCase {
    func testSetRadius() throws {
        let view = UIView.init()
        view.cornerRadius = 11
        assert(view.layer.cornerRadius == 11)
        assert(view.cornerRadius == 11)
    }
    
    func testIdentifiersAndNibName() throws {
        assert(ResultItemGridCell.identifier == "ResultItemGridCell")
        assert(ResultsSectionHeader.identifier == "ResultsSectionHeader")
        assert(ResultItemGridCell.nibName == "ResultItemGridCell")
        assert(ResultsSectionHeader.nibName == "ResultsSectionHeader")
        assert(CategoryTableViewCell.identifier == "CategoryTableViewCell")
        assert(CategoryTableViewCell.nibName == "CategoryTableViewCell")
    }
    
    func testShowErrorMessage() throws {
        let viewController = MockeVC(testCase: self)
        viewController?.showErrorMessage("Error Message")
        assert(viewController?.presentedViewMessage == "Error Message")
    }
    
}

class MockeVC: UIViewController, Mock {
    let callHandler: CallHandler
    var presentedViewMessage: String?
    
    init?(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func instanceType() -> MockeVC {
        return self
    }
    
    override func present(_ viewControllerToPresent: UIViewController,
                          animated flag: Bool, completion: (() -> Void)? = nil) {
        presentedViewMessage = (viewControllerToPresent as? UIAlertController)?.message
    }
}

extension MockeVC: UICollectionViewDataSource,
                   UICollectionViewDelegateFlowLayout, UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell.init()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.init(width: 10, height: 10)
    }
}
