//
//  TestSelectCategoriesModule.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Mockit

class TestSelectCategoriesModule: XCTestCase {
    var presenter: MockSelectCategoriesPresenter!
    var view: MockSelectCategoriesView!
    
    override func setUpWithError() throws {
        view = MockSelectCategoriesView.init(testCase: self)
        presenter = MockSelectCategoriesPresenter.init(view: view, testCase: self)
    }
    
    func testStartWithoutSelectedCategories() {
        let interactor = SelectCategoriesInteractor(presenter: presenter, selectedCategories: [])
        interactor.loadCategories()
        view.verify(verificationMode: Once()).updateCategories(categories: EntityType.alltypes)
        presenter.verify(verificationMode: Once())
            .interactor(didLoadUpdatedCategories: EntityType.alltypes)
    }
    
    func testStartWithSelectedCategories() {
        let selectedCategories = ["Album", "Song"]
        let expected = EntityType.alltypes
            .map({ EntityType(selected: selectedCategories.contains($0.title), title: $0.title) })
        let filteredCategories = selectedCategories.map({ EntityType(selected: true, title: $0) })
        let interactor = SelectCategoriesInteractor(presenter: presenter,
                                                    selectedCategories: filteredCategories)
        interactor.loadCategories()
        view.verify(verificationMode: Once())
            .updateCategories(categories: expected)
        presenter.verify(verificationMode: Once())
            .interactor(didLoadUpdatedCategories: expected)
    }
    
    func testSelection() {
        let expected = EntityType.alltypes.map({ return EntityType(selected: $0.title == "Album", title: $0.title) })
        let interactor = SelectCategoriesInteractor(presenter: presenter, selectedCategories: [])
        interactor.didSelectCategory(at: EntityType.alltypes.firstIndex(where: { $0.title == "Album" }) ?? 0)
        view.verify(verificationMode: Once()).updateCategories(categories: expected)
        presenter.verify(verificationMode: Once())
            .interactor(didLoadUpdatedCategories: expected)
        interactor.didCompleteSelection()
        view.verify(verificationMode: Once())
            .backToSearchScreen(selectedCategories: expected.filter({ $0.selected }))
        presenter.verify(verificationMode: Once())
            .interactor(didGetSelectedCategories: expected.filter({ $0.selected }))
    }
    
    func testUnSelect() {
        let selectedCategories = ["Album", "Song"].map({ EntityType(selected: true, title: $0) })
        let expected = EntityType.alltypes.map({
            return EntityType(selected: $0.title == "Song", title: $0.title)
        })
        let interactor = SelectCategoriesInteractor(presenter: presenter,
                                                    selectedCategories: selectedCategories)
        let selectedIndex = EntityType.alltypes.firstIndex(where: { $0.title == "Album" }) ?? 0
        interactor.didSelectCategory(at: selectedIndex )
        view.verify(verificationMode: Once()).updateCategories(categories: expected)
        presenter.verify(verificationMode: Once())
            .interactor(didLoadUpdatedCategories: expected)
        interactor.didCompleteSelection()
        view.verify(verificationMode: Once())
            .backToSearchScreen(selectedCategories: expected.filter({ $0.selected }))
        presenter.verify(verificationMode: Once())
            .interactor(didGetSelectedCategories: expected.filter({ $0.selected }))
    }
    
    func testConfigurator() throws {
        guard let viewController = SelectCategoriesViewController.loadFromNib() else { assert(false) }
        SelectCategoriesConfiguratorImpl()
            .configureModule(viewController: viewController, selectedCategories: [], delegate: nil)
        assert(viewController.interactor != nil)
    }
    
    func testViewController() throws {
        guard let viewController = SelectCategoriesViewController.loadFromNib() else { assert(false) }
        let interactor = MockInteractor(testCase: self)
        viewController.interactor = interactor
        viewController.loadViewIfNeeded()
        interactor.verify(verificationMode: Once())
            .loadCategories()
        viewController.done(sender: UIBarButtonItem())
        interactor.verify(verificationMode: Once()).didCompleteSelection()
        let indexPath = IndexPath.init(item: 1, section: 2)
        viewController.tableView(viewController.categoriesTableView, didSelectRowAt: indexPath)
        interactor.verify(verificationMode: Once()).didSelectCategory(at: indexPath.item)
        viewController.updateCategories(categories: EntityType.alltypes)
        let expectedCount = EntityType.alltypes.count
        assert(viewController.tableView(viewController.categoriesTableView,
                                        numberOfRowsInSection: 0) == expectedCount)
    }
}

class MockSelectCategoriesPresenter: SelectCategoriesPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: SelectCategoriesView, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view)
    }
    
    func instanceType() -> MockSelectCategoriesPresenter {
        return self
    }
    
    override func interactor(didLoadUpdatedCategories categories: [EntityType]) {
        super.interactor(didLoadUpdatedCategories: categories)
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: categories)
    }
    
    override func interactor(didGetSelectedCategories selectedCategories: [EntityType]) {
        super.interactor(didGetSelectedCategories: selectedCategories)
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: selectedCategories)
    }
    
}

class MockSelectCategoriesView: SelectCategoriesView, Mock {    
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockSelectCategoriesView {
        return self
    }
    
    func updateCategories(categories: [EntityType]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: categories)
    }
    
    func backToSearchScreen(selectedCategories: [EntityType]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: selectedCategories)
    }
}

class MockInteractor: SelectCategoriesInteractorProtocol, Mock {
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockInteractor {
        return self
    }
    
    func didSelectCategory(at index: Int) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: index)
    }
    
    func loadCategories() {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func didCompleteSelection() {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: nil)
    }
}
