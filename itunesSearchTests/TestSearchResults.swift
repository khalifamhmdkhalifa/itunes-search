//
//  TestSearchResults.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Mockit

class TestSearchResults: XCTestCase {
    var presenter: MockSearchResultsPresenter!
    var view: MockSearchResultsView!
    var searchingEntities: [EntityType] = EntityType.alltypes
    var expextedSections: [Section] = []
    var interactor: SearchResultsInteractorImpl!
    
    override func setUpWithError() throws {
        view = MockSearchResultsView.init(testCase: self)
        updateExpectedResults()
        presenter = MockSearchResultsPresenter.init(view: view, testCase: self)
        interactor = SearchResultsInteractorImpl.init(presenter: presenter, results: expextedSections)
    }
    
    func testloading() {
        interactor.loadResults()
        view.verify(verificationMode: Once()).updateDisplayedResults(expextedSections)
        presenter.verify(verificationMode: Once()).interactor(didLoadResults: expextedSections)
    }
    
    func testSelection() {
        let section = Int.random(in: 0..<expextedSections.count)
        let row = Int.random(in: 0..<expextedSections[section].items.count)
        interactor.selectItem(at: IndexPath(item: row, section: section))
        view.verify(verificationMode: Once())
            .showDetailsScreen(item: expextedSections[section].items[row])
        presenter.verify(verificationMode: Once())
            .interactor(didLoadSelectedItem: expextedSections[section].items[row])
    }
    
    func testGridPresentationStyle() {
        interactor.selectPersentatiionStyle(.grid)
        view.verify(verificationMode: Once()).updatePresentationStyle(.grid)
        presenter.verify(verificationMode: Once()).interactor(didUpdatePresentationStyle: .grid)
    }
    
    func testListPresentationStyle() {
        interactor.selectPersentatiionStyle(.list)
        view.verify(verificationMode: Once()).updatePresentationStyle(.list)
        presenter.verify(verificationMode: Once()).interactor(didUpdatePresentationStyle: .list)
    }
    
    func testConfigurator() throws {
        guard let viewController = SearchResultsViewController.loadFromNib() else { assert(false) }
        SearchResultsConfigurator().configure(viewController: viewController, sections: expextedSections)
        assert(viewController.interactor != nil)
    }
    
    func testViewController() throws {
        guard let viewController = SearchResultsViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        let presenter = MockSearchResultsPresenter(view: viewController, testCase: self)
        let interactor = SearchResultsInteractorImpl(presenter: presenter, results: expextedSections)
        viewController.interactor = interactor
        viewController.loadViewIfNeeded()
        presenter.verify(verificationMode: Once())
            .interactor(didLoadResults: expextedSections)
        let indexPath = IndexPath(row: 0, section: 1)
        viewController.collectionView(viewController.resultsCollection, didSelectItemAt: indexPath)
        viewController.updateDisplayedResults(expextedSections)
        presenter.verify(verificationMode: Once())
            .interactor(didLoadSelectedItem: expextedSections[indexPath.section].items[indexPath.item])
        assert(viewController.numberOfSections(in: viewController.resultsCollection) == expextedSections.count)
    }
    
    func testPresentarionStyle() {
        guard let viewController = SearchResultsViewController.loadFromNib() else { assert(false) }
        let presenter = MockSearchResultsPresenter(view: viewController, testCase: self)
        let interactor = SearchResultsInteractorImpl(presenter: presenter,
                                                     results: expextedSections)
        viewController.interactor = interactor
        viewController.loadViewIfNeeded()
        viewController.didSelectGrid(nil)
        presenter.verify(verificationMode: Once()).interactor(didUpdatePresentationStyle: .grid)
        let width = (Float(viewController.resultsCollection.frame.width) -
                        (SearchResultsViewController.minimumHorizontalSpacing*3)) / 3.0
        let size = CGSize.init(width: Int(width), height: 150)
        assert(viewController.collectionView(viewController.resultsCollection,
                                             layout: viewController.resultsCollection.collectionViewLayout,
                                             sizeForItemAt: IndexPath(row: 0, section: 0)) == size)
        viewController.didSelectList(nil)
        let listSize = CGSize(width: viewController.resultsCollection.frame.width, height: 150)
        assert(viewController.collectionView(viewController.resultsCollection,
                                             layout: viewController.resultsCollection.collectionViewLayout,
                                             sizeForItemAt: IndexPath(row: 0, section: 0)) == listSize)
        presenter.verify(verificationMode: Once()).interactor(didUpdatePresentationStyle: .list)
    }
    
    func updateExpectedResults() {
        var sections: [Section] = []
        for entity in searchingEntities {
            let items = (1...Int.random(in: (1...10)))
                .map({ Item(entityType: entity.title, wrapperType: "",
                            artistName: "", previewUrl: "", artworkUrl100: "",
                            trackName: "item\($0)") })
            let section = Section(title: entity.title, items: items)
            sections.append(section)
        }
        self.expextedSections = sections
    }
    
}

class MockSearchResultsPresenter: SearchResultsPresenter, Mock {
    let callHandler: CallHandler
    
    init(view: SearchResultsView, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view)
    }
    
    func instanceType() -> MockSearchResultsPresenter {
        return self
    }
}

class MockSearchResultsView: SearchResultsView, Mock {
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func instanceType() -> MockSearchResultsView {
        return self
    }
    
    func updateDisplayedResults(_ sections: [Section]) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: sections)
    }
    
    func updatePresentationStyle(_ style: PresentationStyle) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: style)
    }
    
    func showDetailsScreen(item: Item) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: item)
    }
}
