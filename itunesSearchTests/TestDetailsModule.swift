//
//  TestDetailsModule.swift
//  itunesSearchTests
//
//  Created by khalifa on 1/2/21.
//

import XCTest
import Mockit

class TestDetailsModule: XCTestCase {
    var presenter: MockDetailsPresenetr!
    var view: MockDetailsView!
    
    override func setUpWithError() throws {
        view = MockDetailsView.init(testCase: self)
        presenter = MockDetailsPresenetr.init(view: view, testCase: self)
        
    }
    
    func getItem(withImage: Bool, withVideo: Bool, withName: Bool = true) -> Item {
        return Item(entityType: "", wrapperType: "", artistName: "artistName",
                    previewUrl: withVideo ? "https://www.google.com" : nil,
                    artworkUrl100: withImage ? "www.google.com" : nil, trackName: withName ? "Name1" : nil)
    }
    
    func testLoadWithoutVideo() {
        let item = getItem(withImage: true, withVideo: false)
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.loadItem()
        view.verify(verificationMode: Once()).hidePlayVideoButton()
        presenter.verify(verificationMode: Once()).interactor(didLoadItem: item)
    }
    
    func testLoadWithVideo() {
        let item = getItem(withImage: true, withVideo: true)
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.loadItem()
        view.verify(verificationMode: Once()).showPlayVideoButton()
        presenter.verify(verificationMode: Once()).interactor(didLoadItem: item)
    }
    
    func testPlayVideo() throws {
        let item = getItem(withImage: true, withVideo: true)
        guard let url = try item.previewUrl?.asURL() else { assert(false) }
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.getVideoUrl()
        view.verify(verificationMode: Once()).showVideoScreen(url)
    }
    
    func testLoadImage() throws {
        let item = getItem(withImage: true, withVideo: true)
        guard let url = try item.artworkUrl100?.asURL() else { assert(false) }
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.loadItem()
        view.verify(verificationMode: Once()).loadImage(url: url)
        presenter.verify(verificationMode: Once()).interactor(didLoadItem: item)
    }
    
    func testDefaultImage() throws {
        let item = getItem(withImage: false, withVideo: true)
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.loadItem()
        view.verify(verificationMode: Once()).showDefaultImage()
        presenter.verify(verificationMode: Once()).interactor(didLoadItem: item)
    }
    
    func testNoTrackName() throws {
        let item = getItem(withImage: false, withVideo: true, withName: false)
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        interactor.loadItem()
        view.verify(verificationMode: Once()).updateTitle(title: item.artistName)
        presenter.verify(verificationMode: Once()).interactor(didLoadItem: item)
    }
    
    func testConfigurator() throws {
        guard let viewController = DetailsViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        DetailsConfigurator().configure(viewController: viewController, item: getItem(withImage: true, withVideo: true))
        assert(viewController.interactor != nil)
    }
    
    func testVC() {
        guard let viewController = DetailsViewController.loadFromNib() else { assert(false) }
        viewController.loadViewIfNeeded()
        viewController.updateTitle(title: "Title2")
        assert(viewController.titleLabel.text == "Title2")
        viewController.hidePlayVideoButton()
        assert(viewController.playButton.isHidden == true)
        viewController.showPlayVideoButton()
        assert(viewController.playButton.isHidden == false)
        let item = getItem(withImage: false, withVideo: true, withName: false)
        guard let url = try? item.previewUrl?.asURL() else { assert(false) }
        viewController.interactor = DetailsInteractorImpl(presenter: presenter,
                                                          item: item)
        viewController.didClickPlayVideo(url)
        presenter.verify(verificationMode: Once()).interactor(didGetVideoUrl: url)
    }
}

class MockDetailsPresenetr: DetailsPresenetr, Mock {
    let callHandler: CallHandler
    
    init(view: DetailsView, testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
        super.init(view: view)
    }
    
    func instanceType() -> MockDetailsPresenetr {
        return self
    }
}

class MockDetailsView: DetailsView, Mock {
    let callHandler: CallHandler
    
    init(testCase: XCTestCase) {
        callHandler = CallHandlerImpl(withTestCase: testCase)
    }
    
    func updateTitle(title: String?) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: title)
    }
    
    func loadImage(url: URL) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: url)
    }
    
    func showDefaultImage() {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func showPlayVideoButton() {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func hidePlayVideoButton() {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: nil)
    }
    
    func showVideoScreen(_ url: URL) {
        callHandler.accept(nil, ofFunction: #function,
                           atFile: #file, inLine: #line, withArgs: url)
    }
    
    func instanceType() -> MockDetailsView {
        return self
    }
}
