//
//  SceneDelegate.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {
    var window: UIWindow?
    
    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let window = UIWindow(windowScene: windowScene)
        let viewController = SearchViewController.loadFromNib()
        if let viewController = viewController {
            SearchConfigurator().configure(viewController: viewController)
        }
        window.rootViewController = UINavigationController(rootViewController: viewController ?? UIViewController())
        self.window = window
        window.makeKeyAndVisible()
    }
}
