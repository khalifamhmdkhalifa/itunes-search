//
//  AlamofireAdapter.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit
import Alamofire

class AlamofireAdaptor: NetworkingInterface {
    let requestBaseURL: String
    let requestHeaders: [String: String]?
    let minValidStatusCode = 200
    let maxValidStatusCode = 403
    let AlamofireBugErrorCode = 3840
    let validContentTypes = ["application/json",
                             "Accept", "application/hal+json", "text/javascript"]
    
    init(baseURL: String, headers: [String: String]? = nil) {
        self.requestBaseURL = baseURL
        self.requestHeaders = headers
    }
    
    func request<T: Decodable>(_ specs: RequestSpecs<T>,
                               completionBlock: @escaping (T?, Error?) -> Void) {
        let url = requestBaseURL + specs.urlString
        let encoding = convertRequestEnconding(specs.encoding)
        let request = AF.request(url, method: convertRequestMethodToAlamofireMethod(specs.method),
                                 parameters: specs.parameters, encoding: encoding,
                                 headers: HTTPHeaders.init(requestHeaders ?? [:]))
            .validate(contentType: self.validContentTypes)
        request.responseDecodable(of: T.self) { result in
            if let requestURL = result.request?.url?.absoluteString {
                print("Request - \(requestURL)")
            }
            guard let data = result.value else {
                completionBlock(nil, result.error)
                return
            }
            completionBlock(data, nil)
        }
    }
    
    func convertRequestEnconding(_ enconding: Encoding) -> ParameterEncoding {
        switch enconding {
        case .json:
            return JSONEncoding.default
        case.urlEncodedInURL:
            return URLEncoding.default
        }
    }
    
    func convertRequestMethodToAlamofireMethod(_ method: RequestMethod) -> Alamofire.HTTPMethod {
        switch method {
        case .DELETE:
            return HTTPMethod.delete
        case .GET:
            return HTTPMethod.get
        case .PUT:
            return HTTPMethod.put
        case .POST:
            return HTTPMethod.post
        }
    }
}
