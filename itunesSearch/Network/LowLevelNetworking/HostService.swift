//
//  APIServiceSettings.swift
//  ios-task
//
//  Created by Khalifa on 10/3/18.
//  Copyright © 2018 khalifa. All rights reserved.
//

import UIKit

struct HostService {
    static func getBaseURL() -> String {
        return "https://itunes.apple.com/"
    }
    static var headers: [String: String] { [:] }
    
}
