//
//  ResultItemCell.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit
import SDWebImage

class ResultItemGridCell: UICollectionViewCell {
    @IBOutlet weak var titlelabel: UILabel!
    @IBOutlet weak var image: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        image.image = nil
        image.sd_cancelCurrentImageLoad()
    }
    
    func configure(item: Item) {
        titlelabel.text = item.trackName ?? item.artistName
        image.image = UIImage(named: "img")
        if let url = try? item.artworkUrl100?.asURL() {
            image.sd_imageIndicator = SDWebImageActivityIndicator.gray
            image.sd_setImage(with: url, completed: nil)
        }
    }
}
