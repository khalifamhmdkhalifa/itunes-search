//
//  SearchResultsViewController.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

class SearchResultsViewController: UIViewController {
    @IBOutlet weak var resultsCollection: UICollectionView! {
        didSet {
            resultsCollection.registerHeader(type: ResultsSectionHeader.self)
            resultsCollection.register(type: ResultItemGridCell.self)
            resultsCollection.register(type: ResultItemListCell.self)
        }
    }
    @IBOutlet weak var gridViewContainer: UIView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(didSelectGrid(_:)))
            gridViewContainer.addGestureRecognizer(tap)
        }
    }
    @IBOutlet weak var listViewContainer: UIView! {
        didSet {
            let tap = UITapGestureRecognizer(target: self, action: #selector(didSelectList(_:)))
            listViewContainer.addGestureRecognizer(tap)
        }
    }
    private var sections: [Section] = []
    var interactor: SearchResultsInteractor?
    static let minimumHorizontalSpacing: Float = 8
    private var presentationStyle: PresentationStyle = .grid
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.loadResults()
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 40, height: 40))
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "img2")
        navigationItem.titleView = imageView
    }
    
    @objc func didSelectGrid(_ sender: UITapGestureRecognizer? = nil) {
        interactor?.selectPersentatiionStyle(.grid)
    }
    
    @objc func didSelectList(_ sender: UITapGestureRecognizer? = nil) {
        interactor?.selectPersentatiionStyle(.list)
    }
}
extension SearchResultsViewController: SearchResultsView {
    func showDetailsScreen(item: Item) {
        guard let viewController = DetailsViewController.loadFromNib() else { return }
        DetailsConfigurator().configure(viewController: viewController, item: item)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func updatePresentationStyle(_ style: PresentationStyle) {
        self.presentationStyle = style
        resultsCollection.reloadData()
    }
    
    func updateDisplayedResults(_ sections: [Section]) {
        self.sections = sections
        resultsCollection.reloadData()
    }
    
}

extension SearchResultsViewController: UICollectionViewDelegateFlowLayout,
                                       UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch presentationStyle {
        case .grid:
            let width = (Float(collectionView.frame.width) -
                            (SearchResultsViewController.minimumHorizontalSpacing*3)) / 3.0
            return CGSize.init(width: Int(width), height: 150)
        case .list:
            return CGSize.init(width: collectionView.frame.width, height: 150)
        }
      
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView: ResultsSectionHeader? = collectionView.headerView(for: indexPath)
            headerView?.configure(title: sections[indexPath.section].title)
            return headerView ?? UICollectionReusableView()
        default:
            assert(false, "Unexpected element kind")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        interactor?.selectItem(at: indexPath)
    }
    
}

extension SearchResultsViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sections.count
    }
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return sections[section].items.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch presentationStyle {
        case .grid:
            let cell: ResultItemGridCell? = collectionView.cell(for: indexPath)
            cell?.configure(item: sections[indexPath.section].items[indexPath.row])
            return cell ?? UICollectionViewCell()
        case .list:
            let cell: ResultItemListCell? = collectionView.cell(for: indexPath)
            cell?.configure(item: sections[indexPath.section].items[indexPath.row])
            return cell ?? UICollectionViewCell()
        }
    }
}
