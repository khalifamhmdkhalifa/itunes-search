//
//  SearchResultsPresenter.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SearchResultsView: class {
    func updateDisplayedResults(_ sections: [Section])
    func updatePresentationStyle(_ style: PresentationStyle)
    func showDetailsScreen(item: Item)
}

class SearchResultsPresenter: SearchResultsInteractorOutput {
    private weak var view: SearchResultsView?
    
    init(view: SearchResultsView) {
        self.view = view
    }
    
    func interactor(didLoadSelectedItem item: Item) {
        view?.showDetailsScreen(item: item)
    }
    
    func interactor(didUpdatePresentationStyle style: PresentationStyle) {
        view?.updatePresentationStyle(style)
    }
    
    func interactor(didLoadResults results: [Section]) {
        view?.updateDisplayedResults(results)
    }
}
