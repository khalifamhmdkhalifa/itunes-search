//
//  PresentationStyle.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

enum PresentationStyle: Int, Equatable {
    case list, grid
}
