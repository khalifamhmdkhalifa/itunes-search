//
//  SearchResultsConfigurator.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

class SearchResultsConfigurator {
    
    func configure(viewController: SearchResultsViewController, sections: [Section]) {
        let presenter = SearchResultsPresenter(view: viewController)
        let interactor = SearchResultsInteractorImpl(presenter: presenter, results: sections)
        viewController.interactor = interactor
    }
    
}
