//
//  SearchResultsInteractor.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SearchResultsInteractorOutput {
    func interactor(didLoadResults results: [Section])
    func interactor(didUpdatePresentationStyle style: PresentationStyle)
    func interactor(didLoadSelectedItem item: Item)
}

protocol SearchResultsInteractor {
    func loadResults()
    func selectPersentatiionStyle(_ style: PresentationStyle)
    func selectItem(at indexPath: IndexPath)
}

class SearchResultsInteractorImpl: SearchResultsInteractor {
    private var presenter: SearchResultsInteractorOutput
    private var results: [Section]
    private var presentationStyle: PresentationStyle = .grid
    
    init(presenter: SearchResultsInteractorOutput, results: [Section]) {
        self.presenter = presenter
        self.results = results
    }
    
    func loadResults() {
        presenter.interactor(didLoadResults: results)
    }
    
    func selectPersentatiionStyle(_ style: PresentationStyle) {
        presenter.interactor(didUpdatePresentationStyle: style)
    }
    
    func selectItem(at indexPath: IndexPath) {
        presenter.interactor(didLoadSelectedItem: results[indexPath.section].items[indexPath.row])
    }
}
