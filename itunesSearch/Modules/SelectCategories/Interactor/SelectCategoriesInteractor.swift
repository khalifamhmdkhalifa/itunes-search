//
//  SelectCategoriesInteractor.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SelectCategoriesInteractorOutput {
    func interactor(didLoadUpdatedCategories categories: [EntityType])
    func interactor(didGetSelectedCategories selectedCategories: [EntityType])
}

class SelectCategoriesInteractor {
    private var presenter: SelectCategoriesInteractorOutput
    private var categories: [EntityType]
    
    init(presenter: SelectCategoriesInteractorOutput, selectedCategories: [EntityType]) {
        self.presenter = presenter
        self.categories = EntityType.alltypes.map { item in
            let selected = selectedCategories.contains(where: { $0 == item })
            return EntityType(selected: selected, title: item.title) }
    }
}

extension SelectCategoriesInteractor: SelectCategoriesInteractorProtocol {
    func didCompleteSelection() {
        presenter.interactor(didGetSelectedCategories: categories.filter({ $0.selected }))
    }
    
    func didSelectCategory(at index: Int) {
        categories[index].selected = !categories[index].selected
        presenter.interactor(didLoadUpdatedCategories: categories)
    }
    
    func loadCategories() {
        presenter.interactor(didLoadUpdatedCategories: categories)
    }
}
