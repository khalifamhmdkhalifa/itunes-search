//
//  MediaType.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

struct EntityType: Equatable {
    static let alltypes: [EntityType] = ["Album", "Artist", "Book",
                                         "Movie", "Music Video", "Podcast", "Song"].map {
                                            EntityType(selected: false, title: $0) }
    var selected: Bool
    var title: String
    var code: String {
        switch title {
        case "Album": return "album"
        case "Artist": return "musicArtist"
        case "Book": return "ebook"
        case "Movie": return "movie"
        case "Music Video": return "musicVideo"
        case "Podcast": return "podcast"
        case "Song": return "song"
        default: return ""
        }
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.title == rhs.title
    }
}
