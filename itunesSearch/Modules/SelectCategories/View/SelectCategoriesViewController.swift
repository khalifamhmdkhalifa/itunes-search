//
//  SelectCategoriesViewController.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

protocol SelectCategoriesInteractorProtocol {
    func didSelectCategory(at index: Int)
    func loadCategories()
    func didCompleteSelection()
}

protocol SelectCategoriesDelegate: class {
    func didSelectCategories(_ categories: [EntityType])
}

class SelectCategoriesViewController: UIViewController {
    @IBOutlet weak var categoriesTableView: UITableView! {
        didSet {
            categoriesTableView.register(type: CategoryTableViewCell.self)
        }
    }
    var interactor: SelectCategoriesInteractorProtocol?
    private var categories: [EntityType] = []
    weak var delegate: SelectCategoriesDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Select media type"
        let doneButton = UIBarButtonItem(title: "Done",
                                         style: UIBarButtonItem.Style.plain,
                                         target: self, action: #selector(self.done(sender:)))
        self.navigationItem.rightBarButtonItem = doneButton
        interactor?.loadCategories()
    }
    
    @objc func done(sender: UIBarButtonItem) {
        interactor?.didCompleteSelection()
         _ = navigationController?.popViewController(animated: true)
     }
}
extension SelectCategoriesViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        interactor?.didSelectCategory(at: indexPath.row)
    }
}

extension SelectCategoriesViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: CategoryTableViewCell = tableView.cell() else { return UITableViewCell() }
        cell.configure(category: categories[indexPath.row])
        return cell
    }
}

extension SelectCategoriesViewController: SelectCategoriesView {
    func backToSearchScreen(selectedCategories: [EntityType]) {
        delegate?.didSelectCategories(selectedCategories)
        self.navigationController?.popViewController(animated: true)
    }
    
    func updateCategories(categories: [EntityType]) {
        self.categories = categories
        categoriesTableView.reloadData()
    }
}
