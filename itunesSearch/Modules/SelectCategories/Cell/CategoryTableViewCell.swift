//
//  CategoryTableViewCell.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var checkedImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    func configure(category: EntityType) {
        self.titleLabel.text = category.title
        checkedImage.isHidden = !category.selected
    }
}
