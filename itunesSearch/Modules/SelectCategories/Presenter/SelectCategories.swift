//
//  SelectCategories.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SelectCategoriesView: class {
    func updateCategories(categories: [EntityType])
    func backToSearchScreen(selectedCategories: [EntityType])
}

class SelectCategoriesPresenter: SelectCategoriesInteractorOutput {
    weak var view: SelectCategoriesView?
    
    init(view: SelectCategoriesView?) {
        self.view = view
    }
    
    func interactor(didLoadUpdatedCategories categories: [EntityType]) {
        view?.updateCategories(categories: categories)
    }
    
    func interactor(didGetSelectedCategories selectedCategories: [EntityType]) {
        view?.backToSearchScreen(selectedCategories: selectedCategories)
    }
    
}
