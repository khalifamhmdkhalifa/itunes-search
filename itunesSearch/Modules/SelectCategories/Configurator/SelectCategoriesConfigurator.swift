//
//  SelectCategoriesConfigurator.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

class SelectCategoriesConfiguratorImpl {
    func configureModule(viewController: SelectCategoriesViewController,
                         selectedCategories: [EntityType],
                         delegate: SelectCategoriesDelegate?) {
        let presenter = SelectCategoriesPresenter(view: viewController)
        let interactor = SelectCategoriesInteractor(presenter: presenter, selectedCategories: selectedCategories)
        viewController.interactor = interactor
        viewController.delegate = delegate
    }
}
