//
//  DetailsConfigurator.swift
//  itunesSearch
//
//  Created by khalifa on 1/2/21.
//

import Foundation

class DetailsConfigurator {
    func configure(viewController: DetailsViewController, item: Item) {
        let presenter = DetailsPresenetr(view: viewController)
        let interactor = DetailsInteractorImpl(presenter: presenter, item: item)
        viewController.interactor = interactor
    }
}
