//
//  DetailsInteractor.swift
//  itunesSearch
//
//  Created by khalifa on 1/2/21.
//

import Foundation

protocol DetailsInteractorOutput {
    func interactor(didLoadItem item: Item)
    func interactor(didGetVideoUrl url: URL)
}

protocol DetailsInteractor {
    func loadItem()
    func getVideoUrl()
}

class DetailsInteractorImpl: DetailsInteractor {
    private var presenter: DetailsInteractorOutput
    private var item: Item
    
    init(presenter: DetailsInteractorOutput, item: Item) {
        self.presenter = presenter
        self.item = item
    }
    
    func loadItem() {
        presenter.interactor(didLoadItem: item)
    }
    
    func getVideoUrl() {
        if let urlString = item.previewUrl, let url = URL(string: urlString) {
            presenter.interactor(didGetVideoUrl: url)
        }
    }
}
