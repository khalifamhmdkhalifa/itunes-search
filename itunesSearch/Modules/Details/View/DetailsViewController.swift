//
//  DetailsViewController.swift
//  itunesSearch
//
//  Created by khalifa on 1/2/21.
//

import UIKit
import SDWebImage
import AVKit
import AVFoundation

class DetailsViewController: UIViewController {
    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var interactor: DetailsInteractor?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        interactor?.loadItem()
        self.title = "Details"
    }
    
    @IBAction func didClickPlayVideo(_ sender: Any) {
        interactor?.getVideoUrl()
    }
}

extension DetailsViewController: DetailsView {
    func showVideoScreen(_ url: URL) {
        let player = AVPlayer(url: url)
        let viewController = AVPlayerViewController()
        viewController.player = player
        self.present(viewController, animated: true) { viewController.player?.play() }
    }
    
    func updateTitle(title: String?) {
        self.titleLabel.text = title
    }
    
    func loadImage(url: URL) {
        image.sd_imageIndicator = SDWebImageActivityIndicator.gray
        image.sd_setImage(with: url, completed: nil)
    }
    
    func showDefaultImage() {
        image.image = UIImage(named: "img")
    }
    
    func showPlayVideoButton() {
        playButton.isHidden = false
    }
    
    func hidePlayVideoButton() {
        playButton.isHidden = true
    }
}
