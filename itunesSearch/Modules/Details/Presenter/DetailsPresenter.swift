//
//  DetailsPresenter.swift
//  itunesSearch
//
//  Created by khalifa on 1/2/21.
//

import Foundation

protocol DetailsView: class {
    func updateTitle(title: String?)
    func loadImage(url: URL)
    func showDefaultImage()
    func showPlayVideoButton()
    func hidePlayVideoButton()
    func showVideoScreen(_ url: URL)
}

class DetailsPresenetr {
    private weak var view: DetailsView?
    
    init(view: DetailsView) {
        self.view = view
    }
}

extension DetailsPresenetr: DetailsInteractorOutput {
    func interactor(didLoadItem item: Item) {
        view?.updateTitle(title: item.trackName ?? item.artistName)
        if item.previewUrl != nil {
            view?.showPlayVideoButton()
        } else {
            view?.hidePlayVideoButton()
        }
        if let url = try? item.artworkUrl100?.asURL() {
            view?.loadImage(url: url)
        } else {
            view?.showDefaultImage()
        }
    }
    
    func interactor(didGetVideoUrl url: URL) {
        view?.showVideoScreen(url)
    }
}
