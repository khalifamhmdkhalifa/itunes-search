//
//  SearchConfigurator.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

class SearchConfigurator {
    func configure(viewController: SearchViewController) {
        let presenter = SearchPresenterImpl(view: viewController)
        let service = SearchService(coreNetwork: CoreNetwork.sharedInstance)
        let interactor = SearchInteractorImpl(presenter: presenter,
                                              service: service)
        viewController.interactor = interactor
    }
}
