//
//  CategoryCell.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

class CategoryCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    
    func configure(title: String) {
        self.titleLabel.text = title
    }
}
