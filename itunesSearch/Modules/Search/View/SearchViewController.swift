//
//  SearchViewController.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

class SearchViewController: UIViewController {
    @IBOutlet weak var searchTermField: UITextField!
    @IBOutlet weak var categoriesCollectionHeight: NSLayoutConstraint!
    @IBOutlet weak var categoriesCollection: UICollectionView! {
        didSet {
            categoriesCollection.delegate = self
            categoriesCollection.register(type: CategoryCell.self)
        }
    }
    @IBOutlet weak var loadingView: UIActivityIndicatorView!
    private var categories: [EntityType] = []
    private var sizingLabel: UILabel = UILabel()
    var interactor: SearchInteractor?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        setBackground()
        let height = categoriesCollection.collectionViewLayout.collectionViewContentSize.height
        categoriesCollectionHeight.constant = height
    }
    
    private func setBackground() {
        let fromColor =  UIColor(red: 76.0/255.0, green: 164.0/255.0, blue: 180.0/255.0, alpha: 1)
        let toColor = UIColor(red: 186.0/255.0, green: 209.0/255.0, blue: 250.0/255.0, alpha: 1)
        self.view.gradientBackground(from: fromColor, to: toColor, direction: .topToBottom)
    }
    
    private func reloadCategories() {
        self.categoriesCollection.reloadData()
        let height = max(40, categoriesCollection.collectionViewLayout.collectionViewContentSize.height)
        categoriesCollectionHeight.constant = height
        view.setNeedsLayout()
    }
    
    @IBAction func didClickOnSearch(_ sender: Any) {
        showLoadingView()
        self.interactor?.search(searchTerm: searchTermField.text ?? "", categories: categories)
    }
    
    @IBAction func didClickOnCategories(_ sender: Any) {
        guard let viewController = SelectCategoriesViewController.loadFromNib() else { return }
        SelectCategoriesConfiguratorImpl()
            .configureModule(viewController: viewController,
                             selectedCategories: categories, delegate: self)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}

extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: CategoryCell = collectionView.cell(for: indexPath) else { return UICollectionViewCell() }
        cell.configure(title: categories[indexPath.row].title)
        return cell
    }
}

extension SearchViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        sizingLabel.text = categories[indexPath.row].title
        let width = min(sizingLabel.intrinsicContentSize.width + 10, collectionView.bounds.width)
        let height = sizingLabel.intrinsicContentSize.height + 15
        return CGSize(width: width, height: height)
    }
}

extension SearchViewController: SelectCategoriesDelegate {
    func didSelectCategories(_ categories: [EntityType]) {
        self.categories = categories
        self.reloadCategories()
    }
}

extension SearchViewController: SearchView {
    func hideLoadingView() {
        loadingView.stopAnimating()
    }
    
    func showLoadingView() {
        loadingView.startAnimating()
    }
    
    func showSearchResultsScreen(with results: SearchResult) {
        guard let viewController =  SearchResultsViewController.loadFromNib() else { return }
        SearchResultsConfigurator().configure(viewController: viewController, sections: results.sections)
        navigationController?.pushViewController(viewController, animated: true)
    }
}
