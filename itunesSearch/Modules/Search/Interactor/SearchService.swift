//
//  SearchService.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

class SearchService {
    private var coreNetwork: CoreNetworkProtocol
    private static let termKey = "term"
    private static let entityKey = "entity"
    
    init(coreNetwork: CoreNetworkProtocol) {
        self.coreNetwork = coreNetwork
    }
    
    func search(searchTerm: String, categories: [EntityType],
                completion: @escaping (SearchResult?, String?) -> Void) {
        let finalResult = ValueWrapper<SearchResult>(SearchResult(resultCount: 0, results: []))
        let group = DispatchGroup()
        for category in categories {
            search(in: group, searchTerm: searchTerm, category: category, finalResult: finalResult)
        }
        group.notify(queue: .main, execute: { completion(finalResult.value, finalResult.error) })
    }
    
    private func search(in group: DispatchGroup, searchTerm: String,
                        category: EntityType, finalResult: ValueWrapper<SearchResult>) {
        group.enter()
        let parameters = [SearchService.termKey: searchTerm as AnyObject,
                          SearchService.entityKey: (category.code) as AnyObject]
        let request = RequestSpecs<SearchResult>(method: .GET, urlString: "search", parameters: parameters)
        coreNetwork.makeRequest(request: request, completion: { result, error in
            if var result = result {
                result.results = result.results.map({
                    var item = $0
                    item.entityType = category.title
                    return item
                })
                finalResult.value?.append(result: result)
            } else {
                finalResult.error = error?.localizedDescription
            }
            group.leave()
        })
    }
}

class ValueWrapper<T> {
    var value: T?
    var error: String?
    
    init(_ value: T?) {
        self.value = value
    }
}
