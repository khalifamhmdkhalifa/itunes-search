//
//  SearchInteractor.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SearchInteractor {
    func search(searchTerm: String, categories: [EntityType])
}

protocol SearchInteractorOutput {
    func interactor(didLoadSearchResults results: SearchResult)
    func interactor(didFailToLoadToSearchResults error: String)
}

class SearchInteractorImpl: SearchInteractor {
    private var presenter: SearchInteractorOutput?
    private var service: SearchService
    
    init(presenter: SearchInteractorOutput, service: SearchService) {
        self.presenter = presenter
        self.service = service
    }
    
    func search(searchTerm: String, categories: [EntityType]) {
        guard !searchTerm.isEmpty && !categories.isEmpty else {
            presenter?.interactor(didFailToLoadToSearchResults:
                                    "Please provide a valid value for search term and select one ore more category")
            return
        }
        service.search(searchTerm: searchTerm, categories: categories) { result, _ in
            if let result = result {
                if result.resultCount > 0 {
                    self.presenter?.interactor(didLoadSearchResults: result)
                } else {
                    self.presenter?.interactor(didFailToLoadToSearchResults: "No Results Found")
                }
            }
        }
    }
}
