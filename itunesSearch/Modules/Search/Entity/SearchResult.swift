//
//  SearchResult.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

struct SearchResult: Codable {
    var resultCount: Int
    var results: [Item]
    
    var sections: [Section] {
        var dictionary: [String: [Item]] = [:]
        for item in results {
            if let entityType = item.entityType {
                var list = dictionary[entityType] ?? []
                list.append(item)
                dictionary[entityType] = list
            }
        }
        return dictionary.map({ Section(title: $0, items: $1) })
    }
    
    mutating func append(result: SearchResult) {
        resultCount += result.resultCount
        results.append(contentsOf: result.results)
    }
}

struct Item: Codable, Equatable {
    var entityType: String?
    let wrapperType: String?
    let artistName: String?
    let previewUrl: String?
    let artworkUrl100: String?
    let trackName: String?
    
    enum CodingKeys: String, CodingKey {
        case wrapperType
        case artistName
        case previewUrl
        case artworkUrl100
        case entityType
        case trackName
    }
}

struct Section: Equatable {
    var title: String
    var items: [Item]
}
