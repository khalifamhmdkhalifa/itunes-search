//
//  SearchPresenter.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import Foundation

protocol SearchView: class {
    func showErrorMessage(_ text: String)
    func showSearchResultsScreen(with results: SearchResult)
    func hideLoadingView()
}

class SearchPresenterImpl: SearchInteractorOutput {
    weak var view: SearchView?
    
    init(view: SearchView) {
        self.view = view
    }
    func interactor(didFailToLoadToSearchResults error: String) {
        view?.hideLoadingView()
        view?.showErrorMessage(error)
    }
    
    func interactor(didLoadSearchResults results: SearchResult) {
        view?.hideLoadingView()
        view?.showSearchResultsScreen(with: results)
    }
    
}
