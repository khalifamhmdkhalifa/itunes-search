//
//  UITableViewExtensions.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

extension UITableView {
    func register<T: UITableViewCell>(type: T.Type) {
        register(UINib.init(nibName: T.nibName, bundle: nil), forCellReuseIdentifier: T.identifier)
    }
    
    func cell<T: UITableViewCell>() -> T? {
        return dequeueReusableCell(withIdentifier: T.identifier) as? T
    }
}

extension UITableViewCell {
    class var nibName: String {
        return "\(self)"
    }
    class var identifier: String {
        return "\(self)"
    }
}
