//
//  UICollectionView.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

extension UICollectionView {
    func register<T: UICollectionViewCell>(type: T.Type) {
        register(UINib.init(nibName: T.nibName, bundle: nil),
                 forCellWithReuseIdentifier: T.identifier)
    }
    
    func registerHeader<T: UICollectionReusableView>(type: T.Type) {
        self.register(UINib(nibName: T.nibName, bundle: nil),
                      forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader,
                      withReuseIdentifier: T.identifier)
    }
    
    func cell<T: UICollectionViewCell>(for indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withReuseIdentifier: T.identifier, for: indexPath) as? T
    }
    
    func headerView<T: UICollectionReusableView>(for indexPath: IndexPath) -> T? {
        return self.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader,
                                                     withReuseIdentifier: T.identifier,
                                                     for: indexPath) as? T
    }
}

extension UICollectionReusableView {
    static var identifier: String { "\(self)" }
    class var nibName: String { "\(self)" }
}
