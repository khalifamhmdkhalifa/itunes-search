//
//  ViewControllerExtension.swift
//  itunesSearch
//
//  Created by khalifa on 1/1/21.
//

import UIKit

extension UIViewController {
    class var nibName: String {
        return "\(self)"
    }
    
    static func loadFromNib() -> Self? {
        return Self(nibName: nibName, bundle: nil )
    }
}

extension UIViewController {
    func showErrorMessage(_ text: String) {
        let alert = UIAlertController.init(title: nil, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
